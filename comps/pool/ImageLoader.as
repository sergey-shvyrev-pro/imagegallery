/**
 * Created by sergey on 22.03.14.
 */
package comps.pool {
import flash.display.Bitmap;
import flash.display.BitmapData;
import flash.display.Loader;
import flash.events.Event;
import flash.events.IEventDispatcher;
import flash.events.IOErrorEvent;
import flash.events.SecurityErrorEvent;
import flash.geom.Matrix;
import flash.net.URLRequest;

public class ImageLoader implements IEventDispatcher{

    private var _data:Object;
    private var _width:uint;
    private var _height:uint;
    private var _loader:Loader;
    private var _request:URLRequest;
    private var _image:BitmapData;

    public function ImageLoader() {
        initLoader();
    }

    private function initLoader():void {
        this._loader = new Loader()
        this._loader.contentLoaderInfo.addEventListener(Event.COMPLETE, handler_complete);
        this._loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, handler_error);
        this._loader.contentLoaderInfo.addEventListener(SecurityErrorEvent.SECURITY_ERROR, handler_error);
    }

    private function handler_complete(event:Event):void {
        var bmp:Bitmap = (event.target.content as Bitmap);
        this._image = new BitmapData(this._width, this._height, false, 0x0);
        drawImage(bmp.bitmapData, this._image);

        bmp.bitmapData.dispose();
        bmp = null;
        this._loader.contentLoaderInfo.removeEventListener(Event.COMPLETE, handler_complete);
        this._loader.contentLoaderInfo.removeEventListener(IOErrorEvent.IO_ERROR, handler_error);
        this._loader.contentLoaderInfo.removeEventListener(SecurityErrorEvent.SECURITY_ERROR, handler_error);
    }

    private function drawImage(src:BitmapData, dest:BitmapData):void {
        var scale:Number = Math.max(dest.width/src.width, dest.height/src.height);
        var mtx:Matrix = new Matrix();
        mtx.scale(scale, scale);

        var dx:int = dest.width - Math.floor(scale*src.width) >> 1;
        var dy:int = dest.height - Math.floor(scale*src.height) >> 1;

        mtx.translate(dx, dy);

        dest.draw(src, mtx, null, null, null, true);

        mtx = null;
    }

    public function addEventListener(type:String, listener:Function, useCapture:Boolean = false, priority:int = 0, useWeakReference:Boolean = false):void {
        this._loader.contentLoaderInfo.addEventListener(type, listener, useCapture, priority, useWeakReference);
    }

    public function removeEventListener(type:String, listener:Function, useCapture:Boolean = false):void {
        this._loader.contentLoaderInfo.removeEventListener(type, listener, useCapture);
    }

    public function dispatchEvent(event:Event):Boolean {
        return this._loader.dispatchEvent(event);
    }

    public function hasEventListener(type:String):Boolean {
        return this._loader.hasEventListener(type);
    }

    public function willTrigger(type:String):Boolean {
        return this._loader.willTrigger(type);
    }

    public function get data():Object {
        return _data;
    }

    public function set data(value:Object):void {
        _data = value;
    }

    public function setImageSize(imageWidth:uint, imageHeight:uint):void {
        this._width = imageWidth;
        this._height = imageHeight;
    }

    public function load():void {
        this._request = new URLRequest();
        this._request.url = this._data.url;
        this._loader.load(this._request);
    }

    public function get title():String {
        return this._data.title;
    }

    private function handler_error(event:*):void {
        throw new Error("не удалось загрузить файл " + this._request.url + " " + event.text);
    }

    public function get image():BitmapData {
        return _image;
    }

    public function get width():uint {
        return _width;
    }

    public function set width(value:uint):void {
        _width = value;
    }

    public function get height():uint {
        return _height;
    }

    public function set height(value:uint):void {
        _height = value;
    }
}
}
