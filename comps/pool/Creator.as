/**
 * Created by sergey on 24.03.14.
 */
package comps.pool {
import comps.pool.FileLoader;
import comps.pool.ImageLoader;

import flash.errors.IllegalOperationError;
import flash.utils.getQualifiedClassName;

import mx.core.UIComponent;

import mx.events.FlexEvent;

public class Creator {
    public function getImage(data:Object):ImageLoader {
        throw new IllegalOperationError("абстрактный метод должен быть переопределен в классе " + getQualifiedClassName(this))
        return null;
    }

    public function getFile(url:String):FileLoader {
        throw new IllegalOperationError("абстрактный метод должен быть переопределен в классе " + getQualifiedClassName(this))
        return null;
    }
}
}