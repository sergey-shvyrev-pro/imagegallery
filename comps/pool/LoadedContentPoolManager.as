/**
 * Created by sergey on 24.03.14.
 */
package comps.pool {
import comps.pool.FileLoader;
import comps.pool.ImageLoader;

import flash.utils.Dictionary;

public class LoadedContentPoolManager extends Creator{
    private var _pool:Dictionary = new Dictionary(true);
    private var _thumbWidth:uint;
    private var _thumbHeight:uint;
    public function LoadedContentPoolManager() {
    }

    public function setThumbSize(width:uint, height:uint):void {
        this._thumbWidth = width;
        this._thumbHeight = height;
    }

    override public function getImage(data:Object):ImageLoader {
        var result:ImageLoader = this._pool[data];

        if(!result){
            result = new ImageLoader();
            result.data = data;
            result.setImageSize(this._thumbWidth, this._thumbHeight);
            result.load();
            this._pool[data] = result;
        }

        return result;
    }

    override public function getFile(url:String):FileLoader {
        var result:FileLoader = this._pool[url];

        if(!result){
            result = new FileLoader();
            result.load(url);
            this._pool[url] = result;
        }

        return result;
    }
}
}
