/**
 * Created by sergey on 22.03.14.
 */
package comps.pool {
import by.blooddy.crypto.serialization.JSON;

import flash.events.Event;
import flash.events.IEventDispatcher;
import flash.events.IOErrorEvent;
import flash.events.SecurityErrorEvent;
import flash.net.URLLoader;
import flash.net.URLRequest;

public class FileLoader implements IEventDispatcher{
    private var _urlLoader:URLLoader;
    private var _request:URLRequest;
    private var _data:Array;
    public function FileLoader() {
        initUrlLoader();
    }

    public function addEventListener(type:String, listener:Function, useCapture:Boolean = false, priority:int = 0, useWeakReference:Boolean = false):void {
        this._urlLoader.addEventListener(type, listener, useCapture, priority, useWeakReference);
    }

    private function initUrlLoader():void {
        this._urlLoader = new URLLoader();
        this._urlLoader.addEventListener(Event.COMPLETE, handler_loaded);
        this._urlLoader.addEventListener(IOErrorEvent.IO_ERROR, handler_error);
        this._urlLoader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, handler_error);
    }

    private function handler_loaded(event:Event):void {
        try{
            this._data = by.blooddy.crypto.serialization.JSON.decode(event.target.data);
        }catch (e:Error){
            throw new Error("не удалось десериализовать файл " + this._request.url);
        }
    }

    private function handler_error(event:*):void {
        throw new Error("не удалось загрузить файл " + this._request.url + event.text);
    }

    public function removeEventListener(type:String, listener:Function, useCapture:Boolean = false):void {
        this._urlLoader.removeEventListener(type, listener, useCapture);
    }

    public function dispatchEvent(event:Event):Boolean {
        return this._urlLoader.dispatchEvent(event);
    }

    public function hasEventListener(type:String):Boolean {
        return this._urlLoader.hasEventListener(type);
    }

    public function willTrigger(type:String):Boolean {
        return this._urlLoader.willTrigger(type);
    }

    public function load(source:String):void {
        this._request = new URLRequest();
        this._request.url = source;
        this._urlLoader.load(this._request);
    }

    public function dispose():void {
        this._urlLoader.close();
        this._urlLoader.removeEventListener(Event.COMPLETE, handler_loaded);
        this._urlLoader.removeEventListener(IOErrorEvent.IO_ERROR, handler_error);
        this._urlLoader.removeEventListener(SecurityErrorEvent.SECURITY_ERROR, handler_error);
        this._urlLoader = null;

        this._request = null;
    }

    public function get data():Array {
        return _data;
    }
}
}
